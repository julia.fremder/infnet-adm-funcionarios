# Funcionalidades

## Funcionários

- Criar cadastro do novo funcionário
- Deletar cadastro do funcionário
- Listar todos os funcionários

# Ao decorrer dos estudos serão implementadas novas funcionalidade

Separar as tabelas no banco de dados e criar os relacionamentos conforme necessário.

- Alterar dados dos funcionários
- Autenticação

# Para poder visualizar o funcionamento deve-se:

1- yarn -> para instalar todas as dependencias.
2- yarn start -> para startar a aplicação
3- npx sequelize-cli db:create -> para criar a tabela de dados
4 - npx sequelize-cli db:migrate -> para criar as migrations no banco de dados

Caso queria visualizar somente a api na pasta raiz do respositório há um arquivo 
do insomina, com os métodos, basta somente importar.