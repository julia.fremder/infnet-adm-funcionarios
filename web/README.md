# Iniciando a aplicação
1. yarn -> para instalar todas as dependências;
2. yarn start -> para startar a aplicação;

## Para visualizar a aplicação em conjunto com o server
1. yarn start -> no server, pois somente com o server funcionando que poderá puxar
as informações lá contidas.

Futuramente quando o server e o web estarão em produção para poderem funcionarem
independentes.
