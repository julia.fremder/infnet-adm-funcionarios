import http from '../config/http'

export const getServiceAllStacks = () => http.get('/stack')

export const getServiceAllStacksListById = (id) => http.get(`/stack/${id}/`)

export const postServiceAllStacksList = (id, data) => http.post(`/stack/${id}/allocation/`, data)

export const deleteServiceAllStacksList = (id) => http.delete(`/stack/${id}`)



