import { useState } from 'react'
import { IoAdd } from 'react-icons/io5'
import { postServiceAllStacksList } from '../../services/stack.service'

import { Form } from './style'

const FormRegister = ({ id, update }) => {
  const [form, setForm] = useState({})

  const handleChangeList = e => {
    setForm({
      ...form,
      [e.target.name]: e.target.value
    })
  }

  const handleCreateList = async () => {
    await postServiceAllStacksList(id, form)

    setForm({})
    update(true)
  }

  return (
    <>
      <Form onSubmit={handleCreateList}>
        <input
          type="text"
          name="employee_name"
          placeholder="Nome"
          value={form.employee_name || ''}
          onChange={handleChangeList}
        />
        <input
          type="text"
          name="employee_email"
          placeholder="E-mail"
          value={form.employee_email || ''}
          onChange={handleChangeList}
        />
        <input
          type="text"
          name="employee_phone"
          placeholder="Telefone"
          value={form.employee_phone || ''}
          onChange={handleChangeList}
        />
        <input
          type="text"
          name="employee_local"
          placeholder="Local"
          value={form.employee_local || ''}
          onChange={handleChangeList}
        />
        <input
          type="text"
          name="employee_state"
          placeholder="Estado"
          value={form.employee_state || ''}
          onChange={handleChangeList}
        />
        <button type="submit">
          <IoAdd />
        </button>
      </Form>
    </>
  )
}
export default FormRegister
